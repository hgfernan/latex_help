\documentclass[12pt, twoside]{book}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
% Declaring the way I use Biblatex. You should change the params
% depending on %your styles
\usepackage[style=abnt,uniquename=init,giveninits]{biblatex}
\addbibresource{bib0.bib}
\addbibresource{bib1.bib}
\usepackage{filecontents}
% Create bib file for Introduction chapter
\begin{filecontents*}{bib0.bib}
@article{acemoglu2000,
    title={The colonial origins of comparative development: An empirical
investigation},
    author={Acemoglu, Daron and Johnson, Simon and Robinson, James A},
    year={2000},
    institution={National bureau of economic research}
}
@book{acemoglu2012,
    title={Why nations fail: the origins of power, prosperity and
poverty},
    author={Acemoglu, Daron and Robinson, James A and Woren, Dan},
    volume={4},
    year={2012},
    publisher={SciELO Chile}
}
\end{filecontents*}
%
% Create bib file for chapter 1. Note that it is not a requirement to
% have different bib files for each chapter.
\begin{filecontents*}{bib1.bib}
@article{acemoglu2000,
    title={The colonial origins of comparative development: An empirical
investigation},
    author={Acemoglu, Daron and Johnson, Simon and Robinson, James A},
    year={2000},
    institution={National bureau of economic research}
}
@article{ackerberg2006,
    title={Structural identification of production functions},
    author={Ackerberg, Daniel and Caves, Kevin and Frazer, Garth},
    year={2006}
}
%
%Create Introduction
\end{filecontents*}
\begin{filecontents*}{chap0.tex}
%This is how you'd go if you use Biblatex
\begin{refsection}
    \chapter{Introduction}
    This is Chapter ``Introduction'' from included file chap0.tex. \\
    This is a citation for \cite{acemoglu2000} from bib0. \\
    \cite{acemoglu2012} is a citation for the second reference. \\
    The Reference list for introductory chapter appears next. \\
    \printbibliography
    \end{refsection}
\end{filecontents*}
%
% Create Chapter 1
\begin{filecontents*}{chap1.tex}
    \begin{refsection}
    \chapter{chap1}
    This is Chapter 1 from included file chap1.tex. \\
    This is a citation for \cite{acemoglu2000} from bib1. \\
    \cite{ackerberg2006} is a citation for the second reference. \\
    The Reference list for the chapter appears next. \\
    \printbibliography
    \end{refsection}
\end{filecontents*}
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Suppress content not required for this solution %
%\include{chapters/titlepage}                    %
%                                                %
%\mainmatter                                     %
%\tableofcontents                                %
%\listoffigures                                  %
%\listoftables                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\include{chap0}
\include{chap1}
\end{document}
